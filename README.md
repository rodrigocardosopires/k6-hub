# K6 HUB

## Rodando os testes:

### É necessário ter o Docker Desktop e o GIT instalado e rodando.

Para clonar o projeto, abra um terminal PowerShell e digite o seguinte comando:

git clone https://gitlab.com/rodrigocardosopires/k6-hub.git

Com Docker rodando e projeto clonado, entre na pasta do projeto, faça upload dos containers e dispare os testes com os seguintes comandos:

#
cd k6-hub

docker-compose up -d influxdb grafana

docker-compose run k6 run /scripts/<nome_do_arquivo>.js
#

Após a finalização do teste, basta abrir o browser em http://localhost:3000/d/k6/k6-load-testing-results?orgId=1&refresh=5s e verificar o resultado da sua execução em tempo real, com gráficos e métricas de forma clara e simples.

O projeto e o docker-compose configurados podem ser reutilizados em qualquer lugar, sendo necessário apenas criar os scripts na pasta /scripts.

Além disso, o K6 é open-source e já existe uma grande comunidade ajudando com dúvidas. Grandes empresas no mundo já adotaram a ferramenta. 
Seu diferencial é a integração com outras ferramentas e possibilidade de desenvolver códigos simples para os testes de performance.
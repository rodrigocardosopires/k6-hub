import http from 'k6/http';
import { check, sleep } from 'k6';

export const options = {
  stages: [
    { duration: '20s', target: 5 },
    { duration: '20s', target: 100 },
    { duration: '20s', target: 5 },
  ],
};

export default function () {
  const res = http.get('http://teste.serasaauto.com.br');
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(1);
}
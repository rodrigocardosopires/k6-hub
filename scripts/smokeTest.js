//Teste de fumaça

//O teste de fumaça é um teste de carga regular, configurado para carga mínima. 


import http from 'k6/http';
import { check, group, sleep, fail } from 'k6';

export const options = {
  vus: 1, // 1 user looping for 1 minute
  duration: '1m',

  thresholds: {
    http_req_duration: ['p(99)<1500'], // 99% das solicitações devem ser concluídas abaixo de 1,5s
  },
};

const BASE_URL = 'https://homolog.vbauto.info/#/';
const USERNAME = 'rodrigo.pires';
const PASSWORD = 'Rod08781982#';

export default () => {
  const loginRes = http.post(`${BASE_URL}/auth/token/login/`, {
    username: USERNAME,
    password: PASSWORD,
  });

  check(loginRes, {
    'logged in successfully': (resp) => resp.json('access') !== '',
  });

  //const authHeaders = {
  //  headers: {
  //    Authorization: `Bearer ${loginRes.json('access')}`,
  //  },
  //};

  //const myObjects = http.get(`${BASE_URL}/my/crocodiles/`, authHeaders).json();
  //check(myObjects, { 'retrieved crocodiles': (obj) => obj.length > 0 });

  sleep(1);
};
